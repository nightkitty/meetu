# meetu

## 重要更新！！！！！！

本项目已使用TypeScript进行重构，本仓库后续将不再推送更新，新仓库请移至：[Gitee - nightkitty/meetu-ts](https://gitee.com/nightkitty/meetu-ts)

<br/>

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
